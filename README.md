# Docker Lab
This lab helps you to practice a complete Docker build CI/CD pipeline with zero cost. By finishing this lab, you will have a full picture of best practices of how a Docker image should be built, tested, and publish in general, and in Gitlab CI in specific. You will also be practicing bash scripting as well to handle all the pipeline handling.
## Description

- Using my repository and container registry in gitlab
- Stage Test is waiting

## Results

### Pipeline with out stage latest and git-tag
![alternative text](CI_CD_Pipeline_01.png "CI/CD_Pipeline_01")

### Pipeline full stage and job
![alternative text](CI_CD_Pipeline_02.png "CI/CD_Pipeline_02")

### Image with branch_name
![alternative text](Container_Registry_01.png "Image_Registry_01")

### Image with all tag 
![alternative text](Container_Registry_02.png "Image_Registry_02")


# Enjoy!